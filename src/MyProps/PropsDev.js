import React from 'react';

function Hello(props)
{
    return (
       <div>
          <hr/>
              <h4>This is Props test</h4>
              <b>Hi, {props.fullname}</b><br></br>
              <img className="img-width" alt="test images" src={props.url}/>
          
       </div>
    );
}



export default Hello;
