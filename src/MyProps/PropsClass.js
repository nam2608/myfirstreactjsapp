import React, {Component} from 'react';

class Hello2 extends Component {
    render() {
        return (
            <div>
                <hr/>
              <h4>This is Props test 2</h4>
              <b>Hi, {this.props.fullname}</b><br></br>
              <img className="img-width" alt="test images" src={this.props.url}/>
            </div>
        );
    }
}



export default Hello2;