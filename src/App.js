import React, { Component } from 'react';
import './App.css';
import RedirectPage from './RouterExample/RedirectPage';
import Menu from './MyComponents/Menu';
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Link
} from "react-router-dom";

class App extends Component {
  
 

  render() {
    console.log("render App.js");

    return (
      <Router>
        <div>
          {/* <Menu/> */}
          <RedirectPage />

        </div>
      </Router>
    );
  }
}

export default App;
