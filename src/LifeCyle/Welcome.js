import React, { Component } from 'react';
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';

class Welcome extends Component {

  constructor(props) {
    super(props);
    this.state = {
      fullname: "Pham Hoang Nam",
      inputname: "Pham Hoang Nam"
    }
  }
  

  componentWillReceiveProps(nextProps) {
    console.log("componentWillReceiveProps Welcome.js");
  }

  componentWillMount() {
    console.log("componentWillMount Welcome.js");
  }

  componentDidMount() {
    console.log("componentDidMount Welcome.js");
  }

  shouldComponentUpdate(nextProps, nextState) {
    console.log("shouldComponentUpdate Welcome.js: " + nextState.fullname);
    return true;
  }


  componentWillUpdate(nextProps, nextState) {
    console.log("componentWillUpdate Welcome.js: " + nextState.fullname);
  }


  componentDidUpdate(prevProps, prevState) {
    console.log("componentDidUpdate Welcome.js: " + prevState.fullname);
  }

  handleChange = (event) => {
    this.setState({
      inputname: event.target.value,
      fullname: event.target.value
    });
  }

  render() {
    return (
      <div>
        welcome
        <p>Hi, {this.state.fullname}</p>

        <TextField id="outlined-basic" label="Outlined" variant="outlined" onChange={this.handleChange} />

      </div>
    );
  }
}

export default Welcome;