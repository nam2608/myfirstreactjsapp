import React, { Component } from 'react';

class ContentRow extends Component {
    constructor(props) {
        super(props);
        
    }
    
    render() {
        return (
            
              <tr>
                    <td>{this.props.empid}</td>
                     <td>{this.props.code}</td>
                     <td>{this.props.fullname}</td>
                     <td>{this.props.email}</td>
                </tr>
               
            
        );
    }
}

export default ContentRow;