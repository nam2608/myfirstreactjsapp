import React, { Component } from 'react';
import {
    BrowserRouter as Router,
    Switch,
    Route,
    Link,
    useParams
  } from "react-router-dom";
import Contact from './Contact';
import Home from './Home';
import Welcome from '../LifeCyle/Welcome';
import LoadNews from './LoadNews';
import NewsDetails from './NewsDetails';
import CallAPI from './CallAPI';
import Dashboard from '../ui/Dashboard';


class RedirectPage extends Component {
    render() {
        return (
            <div>
                <Switch>
                    <Route path="/home">
                        <Home />
                    </Route>
                    <Route path="/news">
                        <LoadNews />
                    </Route>
                    <Route path="/news-detail/:slug.:id.html" children={<NewsDetails />} />
                    <Route path="/contact">
                        <Contact />
                    </Route>
                    <Route path="/welcome">
                        <Welcome/>
                    </Route>
                    <Route path="/call-api">
                        <CallAPI />
                    </Route>
                    <Route path="/dashboard">
                        <Dashboard/>
                    </Route>
                </Switch>
            </div>

        );
    }
}

export default RedirectPage;