import React, { Component } from 'react';
import dtnews from './dataNews.json';
import {
    useParams
} from "react-router-dom";


class NewsDetails extends Component {

    constructor(props) {
        super(props);
        this.state = {
            newid: 0
        }
    }

    componentDidMount() {
        let { id, slug } = this.props.params;
        
        
        this.setState({
            newid: id
        });
    }

    render() {

        return (
            <div>
                {
                    dtnews.map((v, k) => {
                        if (this.state.newid == v.id) {
                            console.log(v.title);
                            return (
                                <div>
                                    <div className="row">

                                        <div className="col-lg-12">

                                            <img className="card-img-top" src={v.image} alt="Card image cap" />
                                            <div className="card-body">
                                                <h5 className="card-title">{v.title}</h5>
                                                <p className="card-text">{v.content}</p>
                                                <p>
                                                    Overview
                                                    Components and options for laying out your Bootstrap project, including wrapping containers, a powerful grid system, a flexible media object, and responsive utility classes.

                                                    Containers
                                                    Containers are the most basic layout element in Bootstrap and are required when using our default grid system. Containers are used to contain, pad, and (sometimes) center the content within them. While containers can be nested, most layouts do not require a nested container.

                                                    Bootstrap comes with three different containers:

                                                    .container, which sets a max-width at each responsive breakpoint
                                                    .container-fluid, which is width: 100% at all breakpoints
                                                    .container, which is width: 100% until the specified breakpoint
                                                    The table below illustrates how each container’s max-width compares to the original .container and .container-fluid across each breakpoint.

                                                    See them in action and compare them in our Grid example.
                                              </p>

                                            </div>

                                        </div>
                                    </div>

                                </div>
                            );


                        } // End if 

                    })// end map

                }

            </div>
        );






    }
}
export default (props) => (
    <NewsDetails {...props} params={useParams()} />
);
