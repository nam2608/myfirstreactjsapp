import React, { Component } from 'react';
import News from './News';
import dtnews from './dataNews.json';

class LoadNews extends Component {
    render() {
        return (
            <div>
                <div className="row">
                    {
                        dtnews.map((v, k) => {
                            return (
                                <News newid={v.id} title={v.title} image={v.image} content={v.content} />
                            );
                        })
                    }
                </div>
            </div>
        );
    }
}

export default LoadNews;