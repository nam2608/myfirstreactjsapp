import React, { Component } from 'react';
import axios from 'axios';
import ContentRow from './ContentRow';

let getData = () => axios.get('https://5e8ae1e8be5500001689e2cb.mockapi.io/api/v1/Employee')
    .then((res) => res.data)

class CallAPI extends Component {

    constructor(props) {
        super(props);
        this.state = {
            data: null
        }
    }


    componentWillUpdate(nextProps, nextState) {
        if (this.state.data === null) {

            getData().then((res) => {
                this.setState({
                    data: res
                });
            })
        }
    }



    printdata = () => {
        if (this.state.data !== null) {
            return this.state.data.map((v, k) => {
                return (
                    <ContentRow key={k} empid={v.id} code={v.code} fullname={v.fullname} email={v.email} />
                )
            })
        }
    }

    loadData_click = () => {
        getData().then((res) => {
            this.setState({
                data: res,
                isloaddata: 0
            });
        })

    }

    render() {

        return (
            <div>

                <button className="btn btn-primary" onClick={this.loadData_click}>Load Data</button>
                <br /><br />
                <div className="table-responsive">
                    <table className="table">
                        <thead>
                            <tr>
                                <th>No.</th>
                                <th>Code</th>
                                <th>Full name</th>
                                <th>Email</th>
                            </tr>
                        </thead>
                        <tbody>
                            {
                                this.printdata()
                            }

                        </tbody>
                    </table>
                </div>
            </div>
        );
    }
}

export default CallAPI;