import React, { Component } from 'react';

class Contact extends Component {

    constructor(props) {
        super(props);
        this.state={
            fname:"",
            femail: "",
            fmessage:""
        }
    }
    

    submitForm = (event) => {
        event.preventDefault();
        console.log(this.printInfo());
        
    }

    getInfo = (event) => {
        this.setState({
            [event.target.name]: event.target.value
            
          });
    }

    printInfo =  () => {
           let fdata = ""
           fdata += " \n Name: "+ this.state.fname;
           fdata += " \n Email: "+ this.state.femail;
           fdata += " \n Message: "+ this.state.fmessage;
           return fdata;
    }

    render() {
        return (
            <div className="container">
            <div className="row">
              <div className="col-md-6 col-md-offset-3">
                <div className="well well-sm">
                  <form className="form-horizontal" action="/" method="post">
                    <fieldset>
                      <legend className="text-center">Contact us</legend>
                      {/* Name input*/}
                      <div className="form-group">
                        <label className="col-md-3 control-label" htmlFor="name">Name</label>
                        <div className="col-md-9">
                          <input id="fname" name="fname" type="text" placeholder="Your name" className="form-control" onChange={this.getInfo} />
                        </div>
                      </div>
                      {/* Email input*/}
                      <div className="form-group">
                        <label className="col-md-3 control-label" htmlFor="email">Your E-mail</label>
                        <div className="col-md-9">
                          <input id="femail" name="femail" type="text" placeholder="Your email" className="form-control" onChange={this.getInfo} />
                        </div>
                      </div>
                      {/* Message body */}
                      <div className="form-group">
                        <label className="col-md-3 control-label" htmlFor="message">Your message</label>
                        <div className="col-md-9">
                          <textarea className="form-control" id="fmessage" name="fmessage" placeholder="Please enter your message here..." rows={5} defaultValue={""} onChange={this.getInfo} />
                        </div>
                      </div>
                      {/* Form actions */}
                      <div className="form-group">
                        <div className="col-md-12 text-right">
                          <button type="submit" onClick={(event)=> this.submitForm(event)} className="btn btn-primary btn-lg">Submit</button>
                        </div>
                      </div>
                    </fieldset>
                  </form>
                </div>
              </div>
            </div>
          </div>
        );
    }
}

export default Contact;