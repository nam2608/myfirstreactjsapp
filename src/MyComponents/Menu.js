import React, { Component } from 'react';
import { Link } from 'react-router-dom';

class Menu extends Component {
    render() {
        return (
            <div>
                <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
                <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossOrigin="anonymous" />


                <nav className="navbar navbar-expand-sm navbar-light bg-light">
                    <a className="navbar-brand" href="#">Learning ReactJS</a>
                    <button className="navbar-toggler d-lg-none" type="button" data-toggle="collapse" data-target="#collapsibleNavId" aria-controls="collapsibleNavId" aria-expanded="false" aria-label="Toggle navigation">
                        <span className="navbar-toggler-icon" />
                    </button>
                    <div className="collapse navbar-collapse" id="collapsibleNavId">
                        <ul className="navbar-nav mr-auto mt-2 mt-lg-0">
                            <li className="nav-item active">
                                <Link to="/home" className="nav-link">Home</Link>
                            </li>
                            <li className="nav-item">
                                <Link to="/dashboard" className="nav-link">Dashboard</Link>
                            </li>
                            <li className="nav-item">
                                <Link to="/contact" className="nav-link">Contact</Link>
                            </li>
                            <li className="nav-item">
                                <Link to="/news" className="nav-link">News</Link>
                            </li>
                            <li className="nav-item">
                                <Link to="/welcome" className="nav-link"> State</Link>
                            </li>
                            <li className="nav-item">
                                <Link to="/call-api" className="nav-link">Call API</Link>
                            </li>
                        </ul>
                    </div>
                </nav>


                {/* <div className="jumbotron jumbotron-fluid">
                    <div className="container">
                        <h1 className="display-3">Learning ReactJS</h1>
                        <p className="lead">Jumbo helper text</p>
                        <hr className="my-2" />
                        <p>More info</p>
                        <p className="lead">
                            <a className="btn btn-primary btn-lg" href="Jumbo action link" role="button">Find more</a>
                        </p>
                    </div>
                </div> */}



            </div>
        );
    }
}

export default Menu;