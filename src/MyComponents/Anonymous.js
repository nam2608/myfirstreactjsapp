import React from 'react';

// Anonymous Component
var Address = function(){
    return (
       <div>
           <hr></hr>
          <b>This is Anonymous Component block</b>
          <p>Address: 123 Tran Hung Dao Street</p>
          <p>Ward: Pham Ngu Lao</p>
          <p>District: 1</p>
          <p>Province: Ho Chi Minh City</p>
       </div>
    );
 }

 export default Address;