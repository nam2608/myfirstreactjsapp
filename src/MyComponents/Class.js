import React, { Component } from 'react';

class Relation extends Component {
    render() {
        return (
            <div>
                <hr/>
                <b>This is Class Component</b>
                <p>Father</p>
                <p>Mother</p>
                <p>Sister</p>
            </div>
        );
    }
}

export default Relation;
